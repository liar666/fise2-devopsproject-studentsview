# Write unit tests for previous code (CPU/RAM...)

In the last Lab, you have started writing code to connect to a few machines (called later the "MonitorMe*" machines) using SSH, to extract data about the health of these machines: CPU usage, Memory usage, Hard Disk usage, network traffic, etc.

In this Lab, you'll write **Unit Tests** to test the code you have written in the previous Lab. You will also **activate these tests**, i.e. make so your GitLab's CI executes these tests automagically everytime you commit a new piece of code in your repository. This will ensure that introducing new code does not break existing code. This is a welcome safe net when you're deeply involved in your programming task and can't find the time and/or hindsight to verify everything!

The basic idea to write your Unit Tests is, for each of your `.py` files, to create a companion file in which you'll write methods that run tests for each of the methods in the original file. For instance, if you have a method `tobetested()` in your original file that is assumed to work for integers between `1` and `10`, you will write a method `test_tobetested()` in your tests file that will test the method for: (i) expected values (e.g. 2, 3), (ii) unexpected values (e.g. 0, 11) and (ii) edge cases (e.g. 1, 10).

Of course, you can do this manually (not recommended). You are rather encouraged to use a Unit Test framework, as it will help you organize your files and run the tests. There are plenty of such frameworks in Python (like unittest, PyUnit, Py.test, Nose, etc.). Some of them might require to organize your code in a particular way (e.g. put all the tests files in a particular directory). In this case, follow the instructions and modify your repository accordingly.

## TODO

- Read some docs about Unit Testing in Python
- Choose a Unit Test framework (enventually install & test a few of them)
- **Write tests** for the various methods you wrote during the last Lab
- Read docs about GitLab-CI on how to activate the Unit Tests
- Create a GitLab-CI rule in your [`.gitlab-ci.yml`](.gitlab-ci.yml) file that **activates your Unit Tests each time a `git push` occurs** (remember it might be relevant to execute these tests only if the `git push` occurs on a specific branch)
- You can use the `python:3.8-alpine` base image for the moment
- **At the end of this session, you'll commit your work in your repository and send us a link to the file where you have put your tests. It will be part of the final score of the project.**

## Resources

- [Freecodecamp's intro to Unit Tests in Python](https://www.freecodecamp.org/news/an-introduction-to-testing-in-python/)
- [Chapter on UnitTests in the book "Dive into Python"](https://diveintopython3.net/unit-testing.html)
- [Chapter on UnitTests in the book "The Hitchhiker's Guide to Python"](https://docs.python-guide.org/writing/tests/)
- [O'Reilly Book on Test Driven Development](https://www.oreilly.com/library/view/test-driven-development-with/9781491958698/)
- [unittest (included in every Python distribution)](https://docs.python.org/3/library/unittest.html) == [PyUnit](https://wiki.python.org/moin/PyUnit)
- [Py.test](https://docs.pytest.org/en/latest/) [Py.test introduction](https://pythontesting.net/framework/pytest/pytest-introduction/)
- [Nose](https://nose.readthedocs.io/en/latest/) [Nose introduction](https://pythontesting.net/framework/nose/nose-introduction/)
- [GitLab-CI Doc](https://docs.gitlab.com/ee/ci/index.html)
- [Keyword reference for the `.gitlab-ci.yml` file](https://docs.gitlab.com/ee/ci/yaml/)
- [GitLab-CI Pipeline Editor](https://docs.gitlab.com/ee/ci/pipeline_editor/)
