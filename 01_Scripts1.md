# Scripts to collect data (CPU/RAM/Net/HD)

> NOTE: In this first session, you will work *individually*: each one of you will try to write the Python script. This will allow each of you to grasp the subject and understand the overall objective. From the next session to the end of the project, you will *work in teams*. (At the beginning of the next session, you will have to choose one version of the code by one of the teamates - e.g. the most advanced, the most beautiful, etc. - and push it on the git repo. Then you will all work jointly based on this version).

The purpose of this Lab is to collect data about the health (CPU/RAM usage, etc) on remote machine(s) and gather them to a monitoring machine, where you'll (later) be able to compute some stats and display them to a final user.

This centralized machine will connect to each monitored machine through ssh connexions to collect each piece of data and bring back these informations to the centralized machine.

## Machines to monitor

You have access to the following machines:

| **MonitorMe1**      | **MonitorMe2**      |
|---------------------|---------------------|
| monitorme1.ddns.net | monitorme2.ddns.net |

You can access these machines with the following credentials:

| **username** | **password** |
|--------------|--------------|
| interfadm    | Projet654!   |

## Which informations are useful to monitor

- [`free`](https://linux.die.net/man/1/free): Display amount of free and used memory in the system (check free and available memory)
- [`top`](https://linux.die.net/man/1/top): Display Linux processes
- [`ps`](https://linux.die.net/man/1/ps): Report a snapshot of the current processes
- [`vmstat`](https://linux.die.net/man/8/vmstat): Report virtual memory statistics
- [`ifconfig -a`](https://linux.die.net/man/8/ifconfig): Display all interfaces which are currently available, even if down, watch TX packets (Transmit) et RX packets (Receive)
- `cat /proc/meminfo` and `cat /proc/cpuinfo`: Display information about mem/cpu
- FYI there is also the command [`iotop`](https://linux.die.net/man/1/iotop) which Monitors I/O (stats on Hard Drive throughput/health), but it is not installed by default in GNU/Linux, thus in the machines you have access to.

## Connect to a machine through a terminal emulator using SSH

If you want to have a look at the above mentioned files or test the above commands, you can connect to the MonitorMe* machines using an SSH client.

Once on the remote machine, you can have a look at the files/commands, but you can't process them locally.

In order to process them, you'll need to have a script language. Such a language is Python and you have it on your local machine(s).

Now, your goal is to connect to the remote machine(s) with SSH *using your local Python*, so that you can process *locally* the result of the *remote* execution of the commands.

## Using a Python script to connect to the distant machine and get informations

What you need is to write a Python script that sends SSH commands to the remote machine and brings back the informations on the local machine.

There are various Python libraries that allow to do that, like Fabric, Paramiko, Spur...

These libraries will execute the command remotely and retrieve the stdin/stdout/stderr (the stream where a program reads its input and writes its output & error messages) as strings on the local machine.

Below is an example of Python code using Paramiko:

```python
client = paramiko.SSHClient()
client.load_system_host_keys()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy)

client.connect(hostname, port=port, username=username, password=password)

_, stdout, stderr = client.exec_command(command)
output = stdout.read().decode("utf-8")
for line in output.splitlines():
    print(line)
```

## Pipeline Configuration

Our pipelines are executed on machines (called GitLab Runners) that are
hosted inside the UJM's datacenter. Thus, if we want our pipelines to
access the Web/internet (e.g. to download Python packages with `pip`),
we need to configure the UJM proxy.

This can be done in any linux script with:

```bash
export http_proxy=http://cache.univ-st-etienne.fr:3128/
export https_proxy=http://cache.univ-st-etienne.fr:3128/
export ftp_proxy=http://cache.univ-st-etienne.fr:3128/
```

Adjust your pipeline scripts so that they execute these lines every
time a command needs to access the Web/internet.

## Resources

- [Example GitLab CI template](./examples/.gitlab-ci.yml.template)
- [5 Commands to monitor your servers](https://www.lemagit.fr/conseil/Cinq-commandes-Linux-pour-monitorer-les-performances-des-serveurs)
- [Fabric](http://www.fabfile.org/)
- [SSH: Execute Remote Command or Script in Linux](https://www.shellhacks.com/ssh-execute-remote-command-script-linux/)
- [Paramiko : SSH connexion from Python](http://www.paramiko.org/)
- [How to convert paramiko's output b'..' strings into parseable u'..' strings](https://stackoverflow.com/questions/606191/convert-bytes-to-a-string/606199#606199)
- [Spur](https://pypi.org/project/spur/)
- [pxssh](http://pexpect.sourceforge.net/pxssh.html)
- [pyssh](http://pyssh.sourceforge.net/)
- [conch](https://twistedmatrix.com/projects/conch/)
- [ssh2-python](https://github.com/ParallelSSH/ssh2-python)
