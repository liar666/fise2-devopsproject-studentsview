# Add a web Framework to display the collected data as Graphs & Dashboards

In this session, the goal is to add a web graphical interface to your code, so that users can connect to a URL and see dashboards & graphs that synthetizes the data that your scripts collect.

These graphs should update automatically to display data evolving over time. Thus, the final user has an good idea of the health of her servers in a blink of an eye. 

Remember the examples of expected final results we provided in the introduction?

| Example of Grafana Display | Example of Beats+ELK-Stack Dashboard |
|----------------------------|--------------------------------------|
| <img alt="Example of Grafana Display" src="./imgs/grafana.png" width="95%"/> | <img alt="Example of Beats+ELK-Stack Dashboard" src="./imgs/elk_monitoring.jpg" width="95%"/> |

There are multiple frameworks in Python to create a Web Interface:

- [Django](https://www.djangoproject.com/) is full featured
- [CherryPy](https://cherrypy.dev/) a Minimalist Python Web Framework
- [Flask](https://flask.palletsprojects.com) another micro-framework
- [Tornado](http://www.tornadoweb.org/) a scalable web server and web application framework

There are also multiple frameworks to draw graphs:

- [Plot.ly](https://plot.ly/) very powerful framework with many types of graphs + manages interactivity
- [D3.js](https://www.fullstackpython.com/d3-js.html) the most well known & widely used graph library (originally in JavaScript)
- Web Framework+Graphs [Django+D3.js](https://dreisbach.us/articles/building-dashboards-with-django-and-d3/)
- The new trend (2022) [Streamlit](https://streamlit.io/)


## Recommended approach

In the end, we recommend to use [**Dash**](https://plot.ly/dash/) because it does exactly what we need here: it combines both a Web Framework and a Graph drawing library. [Bokeh](http://bokeh.pydata.org/en/latest/) is an alternative to Dash, with similar features, but requiring a bit of javascript for some graph features).

## Links

- [Dash VS. Bokeh](https://www.sicara.ai/blog/2018-01-30-bokeh-dash-best-dashboard-framework-python)
- [Bokeh Gallery](https://docs.bokeh.org/en/latest/docs/gallery.html#gallery-server-examples)
- [Dash Gallery](https://dash-gallery.plotly.host/Portal/)
- [Plotly's github homepage](https://github.com/plotly)
- [Dash Tutorial](https://www.datacamp.com/community/tutorials/learn-build-dash-python) 
- https://medium.com/better-programming/build-an-interactive-modern-dashboard-using-dash-ab6b34cb515
- https://medium.com/@aliciagilbert.itsimplified/build-stunning-interactive-web-data-dashboards-with-python-plotly-and-dash-fcbdc09ba318
- https://www.youtube.com/watch?v=hSPmj7mK6ng

## TODO

- Choose a dashboard framework
- Create a first dashboard with static data (test data, not real data from the servers)
- Pipe the data from the existing code to the dashboard (real data)
- Create a full dashboard with all the data collected from the servers
