# Scripts to collect data (extract from LOGs)

During the two previous Labs, you have written code both to get CPU/RAM/... information through SSH and to test this code.

You now have a safety net that "guarantees" (if your tests are correct) that any new code that you produce does not break the previous code you had written.

As a consequence, you are now ready to add more functionalities/code to your project.

The goal today is to add the following functionalities to your code: capture a new kind of information from the machines we have to monitor: the health of the Apache server that runs on "MonitorMe1" (monitorme1.ddns.net).

When daemons like Apache (a [webserver](https://en.wikipedia.org/wiki/Webserver)) run, they write (a lot of) information about what they are doing in log files. These files are located in `/var/log/` under GNU/Linux.

For instance the following lines indicate that someone has tried to access the `/` and `/index` files on the website served by this instance of Apache:

```
127.0.0.1 - - [09/Jan/2020:10:35:48 +0000] "GET / HTTP/1.1" 200 11229 "-" "Wget/1.19.4 (linux-gnu)"
127.0.0.1 - - [09/Jan/2020:12:23:57 +0000] "GET /index HTTP/1.1" 404 488 "-" "Wget/1.19.4 (linux-gnu)"
```

This can be used, for instance, to compute stats about which pages are visited or not and by whom. Such information is very important both for the maintainers of the website (operators, or **Ops**), to know if some pages are visited a lot and might need to be distributed over multiple servers, and for business people, for e.g. on a webstore to know if some products are of more interest to consumers than others.

Also, the lines might be 'tagged' with specific strings like `[INFO]` or `[ERROR]` to inform the reader that the line simply provides an information or relates to an error. The "error" lines are, of course, of ultimate interest for the **Ops** to have a precise idea of the health of the daemon, thus of the website.

You can find ideas of metrics to be computed based on Apache logs by looking at the ELK-Stack tools:

![ELK-Stack Example](./imgs/elk_monitoring.jpg)

If you imagine the website is a webshop, then the following metrics can be useful:

- Number of different IPs that connect to the site (if you use a IP location service, you could also display the locations of the visitors on a map): this might the Ops know who their customers are and eventually move their servers closer to the customer to get smaller response time ot the buisiness people to adapt their commercial strategy.
- Count of visits of each individual page: this might alos be useful for the business people to adapt their commercial strategy.
- Count of 404 errors (web pages that does not exist): malevolent users often try to access pages with known vulnerabilities, thus they will scan your site with a lot of pages that do not exist. A high number of 404 errors might be an indication that your site is under attack.

Another information that can be of interest to the Ops is who is connected to the underlying machine. This can be obtained by running the `last` or `who` commands.

## TODO

Your goals in the current Lab are to:

- Add code to your project in order to extract information from the LOGs, in order to get relevant information (both usage - **who** visits **what** pages - & health - are there **errors**).
- Commit the code and verify that the tests from the previous Labs still pass.

## Resources

- [Apache LOG files](https://httpd.apache.org/docs/1.3/logs.html)
- [Generic Python lib to parse log files](https://pypi.org/project/pylogsparser/0.4/)
- [Python lib to parse Apache logs](https://github.com/rory/apache-log-parser)
- [Regular Expressions: Regexes in Python](https://realpython.com/regex-python/)
- [Regular Expressions (RegExes)](https://docs.python.org/3/howto/regex.html)
- [Learn regex the easy way](https://github.com/ziishaned/learn-regex/)
