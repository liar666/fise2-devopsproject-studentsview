# A little theory about Git Branching

> NOTE: In this first (short) Lab, we'll help you set up your environment correctly, so that you can be quickly and efficiently productive in the next Labs.

## In general

Git branches are a way to work on a "copy" of your code, while keeping track of the changes you make. This can be very useful to develop a new feature: you "copy" the current state of your code (== create a branch), then your write new code/modify existing code. If you're satisfied with your code, you can "replace" the initial code with the content of the branch (== merge the branch) so that the code in the branch becomes the new version of your code (while preserving the history of the branch). If you're not satisfied, you can simply drop the branch (== switch back to the initial code and/or delete the branch).
Branches can also be very useful when multiple persons are working on the code. Indeed, each developer can work on its own copy of the project without interfering with (or being interfered by) the modifications of other developers. Only when one of the devs will push its work in the original branch, she will have to worry about possible changes that other developers made in the same pieces of code (and eventually will have to resolve conflicts).

Git allows you to create as many branches as you want, whenever you want. However, over time, very good developers ("gurus") have developed some "best practices" on how to manage (git) branches.

[This article](https://nvie.com/posts/a-successful-git-branching-model/) provides a very good introduction to one of the most known git branching model.

To sum up, they propose to define the following branches:

- **master:** this is the branch where the source code **always reflects a *production-ready* (*stable*) state**. Anyone that connects to your repository and downloads a snapshot of your code (e.g. zip file) should get code from this branch and should enter in possession of a fully tested and functional program. This branch exists from the beginning of the project and never disappears.
- **develop:** this is the branch where the source code **always reflects a state with the latest delivered development changes for the next release**. Some would call this the "integration branch". This is where any automatic nightly builds (alpha & beta versions of the program) are built from. This branch exists from the beginning of the project and never disappears.
- **release:** these branches (there might exist several) support preparation of a **new production release**. They allow for minor bug fixes and preparing meta-data for a release (version number, build dates, etc.), at the same time as clearing the develop branch to receive features for the next big release. Such branches are created from the develop branch when developers want to create a new version of the program.
- **feature:** these branches (there might exist several) are used to develop new features for the upcoming or a distant **future release**.  Such branches are temporary. They are created from the develop branch and are finally either merged back in the develop branch or abandoned.
- **hotfixes:** these are very specific branches. The are similar to release branches in that they are also meant to prepare for a new production release, albeit unplanned. They arise from the necessity to act immediately upon an undesired state of a **live production version** (thus they are branched off from *master*). When a critical bug in a production version must be resolved immediately, a hotfix branch is created from the master branch (that marks the production version).

***However***... This is a really solid yet heavy branching model. Developers need to master `git` a lot, and the process behind this may slow down the development. Gitflow is a complex branching strategy mainly usefull for large teams with high Continuous Development needs and understanding.

Other branching strategies has been proposed through the years. Here is the one **we suggest for this project**, based on [Github Flow](https://blog.programster.org/git-workflows).
The idea is simple: the `master` branch is the main branch, and every feature branch is created and merge from and into `master`, thanks to a Merge Request.

With this flow, the `master` branch keeps stable thanks to the validation of the Merge Request and the automated tests launched by the CI (this will be part of the project don't worry).

### Resources

- [Git CheatSheet](https://www.atlassian.com/git/tutorials/using-branches)
- [Git Branches](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
- [TP Git from Software Engineering Course](https://mootse.telecom-st-etienne.fr/pluginfile.php/36220/mod_resource/content/1/git_tp.pdf)
- [Git Workflow](http://www.eqqon.com/index.php/Collaborative_Github_Workflow) [Other link](https://guides.github.com/introduction/flow/)
- [Complete free Git book](https://git-scm.com/book/en/v2)
- [Git flow comparison](https://blog.programster.org/git-workflows)
- [The ULTIMATE Git problem solver](https://ohshitgit.com/)

## In this project

In this project, you'll create the following branches:

- 'master': the default branch **mandatory**
- 'feature_XXX': everytime you need to develop a new feature or fix a bug, you'll create a new branch from `master`

![Github Flow](imgs/github-flow.png)

## Tools

### Python

You'll write code in Python. Preferably install Python 3, as [Python 2.x is now deprecated](https://pythonclock.org/)).

You might find it useful to install the **[venv](https://docs.python.org/3/library/venv.html)** [package](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/), in order to install external dependencies (pythno libraries you'll install and use later) in a specific directory, thus not messing with your potential current "system" installation of Python (that is used by the OS and might freeze the whole OS if you mess it up, for instance under GNU/Linux).

You can use whatever IDE/Editor (Development Environment) you wish. Some of the most commons are:
- IDLE (included with every Python installation)
- [Spyder](https://www.spyder-ide.org/)
- [PyCharm](https://www.jetbrains.com/pycharm/)
- [Visual Code](https://code.visualstudio.com/docs/languages/python)
- You can also use online editors like [Repl](https://repl.it/), but it is not recommended, as they might not have some of the libraries you'll need and you'll not be able to install them

### Git & Gitlab

You'll use [TSE Gitlab](https://code.telecomste.fr/) both as your Git repository, and to run the CI (Continuous Integration) pipeline/workflow.

- [What is Git](https://en.wikipedia.org/wiki/Git)
- [What is GitLab](https://en.wikipedia.org/wiki/GitLab)
- [Videos on GitLab-CI](https://www.youtube.com/results?search_query=continuous+integration+gitlab)

#### TODO

##### Git Repository Architecture

As previously described, `master` must only contain *stable and tested* versions of your project. To develop a new feature (=whenever you want to add some code), you must create a new feature branch from `master`, and merge it when its ready **through a [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/)** into `master`.

##### GitLab Workflow/Pipeline

Create a simple `.gitlab-ci.yml` file with a single stage "compile". This stage must test syntax validation of your python code. You can use the command `python -m compileall .` in the script part of the stage.

You can find Gitlab CI examples [here](https://docs.gitlab.com/ee/ci/quick_start/) and [here](https://docs.gitlab.com/ee/ci/examples/).

When you file is pushed on Gitlab, it will enable auto execution of the pipeline every time you add new code on your repository.

YAML is a very simple yet efficient text format for configuration files. You can find more details [here](https://en.wikipedia.org/wiki/YAML).

## Additional Resources

- [Video explaining the 3 most famous workflows with Git](https://www.youtube.com/watch?v=ObmFdQCOt3k)
- [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/) [GitFlow](https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html)
- [GitHub Flow](https://guides.github.com/introduction/flow/)
- [GitLab Flow](https://about.gitlab.com/blog/2014/09/29/gitlab-flow/)
- [Git Best Practices](https://sethrobertson.github.io/GitBestPractices/)

[comment]: # (removing-self-advertisement Video explaining Githubflow and howto do a code review with GitLab https://youtu.be/RqPxcBVGDoU)
