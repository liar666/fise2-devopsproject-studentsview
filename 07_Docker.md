# Create a Docker image

## What is Docker?

[`Docker`](https://en.wikipedia.org/wiki/Docker_(software)) (and more generally, *containers*) and `VirtualBox` (more generally *Virtual Machines*) are similar, because they both allow to create a file (resp. the Docker image/container and the Virtual Machine image) that encapsulates everything you need to run your App almost anywhere.

However, they also **significantly differ** as, in `Docker`, even the OS is abstracted, whereas `VirtualBox` will only emulate/virtualize the hardware.

This means that, in `VirtualBox` (VMs), you have a full virtual computer accessible, on which you'll need to install an OS, device drivers, then all the libs & apps you need. With `Docker` (containers) you can assume that you are already running a Kernel and install only the libs & apps.

In `VirtualBox`, almost any OS can be installed, as the emulated hardware is quite standard, thus supported by most of the existing OSes, whereas in Docker the underlying Kernel is "already installed" and is fixed to Linux.

This makes containers "lighter" (no need to include several GB of OS files in the *Docker image* file) but less portable (as there is no Linux kernel that runs by default under Windows and macOS, we need to find a way to run one before being able to run a Docker container on top of it; Thus Docker containers are a little more difficult to run on these OSes).

### Resources

- https://docker-curriculum.com/
- [Full Docker+Kubernetes Course](https://www.youtube.com/watch?v=bhBSlnQcq2k&list=PLwvrYc43l1Mz_c-vV1yVyvFNFZPAleSNE)
- https://www.youtube.com/watch?v=V9IJj4MzZBc
- https://www.youtube.com/watch?v=Q5POuMHxW-0
- https://www.youtube.com/watch?v=3c-iBn73dDE
- https://www.youtube.com/watch?v=p28piYY_wv8
- [Comparison of Containers and VMs 1](https://www.youtube.com/watch?v=L1ie8negCjc)
- [Comparison of Containers and VMs 2](https://www.youtube.com/watch?v=1WnDHitznGY)
- [Docker Security](https://dev.to/gasparev/advanced-docker-how-to-use-secrets-the-right-way-1l67)
- [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
- [Best Practices for Dockerfile](https://github.com/hexops/dockerfile)
- [How to reduce the size of a Docker image](https://medium.com/@avinashkumarmahto51/the-best-practices-to-reduce-the-size-of-docker-image-a31a33ae417a)

## The link between Docker and GitLab

Hundreds of languages, with thousands of libraries each, and almost as much as tools... This is what Gitlab would have to install on its servers to run every possible pipeline depending on your project specificity. Imagine the mess that it would be to maintain such a machine (incompatibilities to solve, size CPU/RAM/HD of the server, keeping it up-to-date, etc.)!

By default GitLab uses a simpler solution: `Docker` containers to run the CI pipeline. By default, Gitlab starts a new container each time it runs a new pipeline. The container is based on image [ruby:2.5](https://hub.docker.com/layers/ruby/library/ruby/2.5/images/sha256-7d2f1ccfb2e416ae99521f12b1fe3d0ad1e2aaf599675eb9cd29cbb5644da54c), an image that includes the basic Linux kernel, a few tools like the ruby and python languages.

When writing your GitLab-CI file (`.gitlab-ci.yml`), you might have noticed, before you could use the commands to run your code, that you needed to add a few preliminary commands to install the Python libraries that your code depends on. That's because you need to "complete" the installation of the "basic" Docker image used by Gitlab (e.g. ensure the `paramiko` library is present) before being able to run your code.

Such an approach has many advantages:

- [MAINTENANCE] lightens the GitLab server installation & maintenance (see above).
- [SECURITY] isolates the code that your pipeline executes from the host (thus you cannot in/voluntarily break the GitLab server).
- [SECURITY] reduces the [attack surface](https://en.wikipedia.org/wiki/Attack_surface) (your code is only vulnerable to its own bugs and bugs in the libraries your Docker image includes).
- [EFFICIENCY] enables parallelization: several steps of the pipeline can be run in parallel in different Docker containers.
- [EFFICIENCY] the GitLab server can throw everything when your CI pipeline has finished executing, thus regains CPU/RAM/HD resources.
- [EFFICIENCY] Gitlab CI uses *Runners* to launch the containers, and those runners can be installed on a different machine than Gitlab itself. This way, you can virtually have an unlimited number of parallel pipelines running at the same time on different runners.
- [CORRECTNESS] reproducibility/debugging: you can run the same code in the exact same conditions and/or know the exact conditions if a bug occurs ([Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_code)).

The main drawback of this is that the extra steps ran at the container start-up (in your case, the installation of Python libraries) are run *everytime* the pipeline is executed, thus they slow down the pipeline execution.

## TODO

Your **goals in this session** is:

- to create a Docker *image* that contains only the libraries you need
- modify your pipeline so that it uses this image, instead of the default one, in order to speed up the execution of your CI pipeline.

Depending on your pipeline stages and the libraries you've chosen to use (external Unit Test library?), you might want to create several Docker images, that do not contain the same sets of libraries.

> **Before you start working on this, please read the following section.**

## Preliminary Exercises

### Set up

You can install `Docker` on your own machine and complete the current Lab locally, but it is more difficult to do if you are under Windows or macOS ([Instructions for Windows](https://docs.docker.com/docker-for-windows/), [Instructions for macOS](https://docs.docker.com/docker-for-mac/)).

You can also use the [VirtualBox image](https://mega.nz/#!wURnlYTR!pyCOzguuRqEtY4hS41pLEjIDEVTx5H01Wb0-S0eZrt8) (we already used previously), it has Docker installed with proper configuration (credentials: user:`user`, password:`user`).

### Basic concepts

### Exo1: Test your installation

Run `docker ps`. This should reply with an empty tabe of containers (obviously, you haven't created any container yet):

```
CONTAINER ID     IMAGE     COMMAND     CREATED     STATUS      PORTS       NAMES
```

If you have permission issues, see [this link](https://docs.docker.com/install/linux/linux-postinstall/).

Run `docker run hello-world`

You should have a message like this:

```
latest: Pulling from library/hello-world
1b930d010525: Pull complete
Digest: sha256:9572f7cdcee8591948c2963463447a53466950b3fc15a247fcad1917ca215a2f
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
```

#### Containers and images

The instance of docker on which your code is running is called a *container*. A container is to docker what a virtual machine is to VirtualBox. As mentioned earlier, a container is like a tiny virtual machine running on your computer. A container can be saved into an *image*, like a snapshot, to create other containers identical to the original container. Basically, when you run `docker run hello-world`, docker downloads the corresponding *image* (`latest: Pulling from library/hello-world`), and then creates a new container based on this image.

#### docker is similar to git (pull push...)

`Docker` as a set of command that reminds `git`, because it manages a repository of images behind the scenes. With `docker pull`, you can get the latest version of a docker image. The `docker commit` command allows you to create an image (snapshot) from an existing container. With `docker push <registry>`, you can send your image on a server for backup/sharing.

### Exo2: Get a basic image and run it as a container

[Dockerhub](https://hub.docker.com/) offers a large variety of pre-created docker images. You can find base images (images of a specific OS without anything special installed on it), and more complex images (like gitlab-ce, containing gitlab itself, ready to be used on a container).

The first step to create your own docker image for the project is to choose a base image. See these links for help.

> Be careful! The size of your image matters. Containers are meant to be small, quickly downloadable and fast to run. If you choose a large image, it will slow down your pipeline. Prefer a small image, and only install what you actually need.

- https://hub.docker.com/search?category=base&source=verified&type=image
- https://hub.docker.com/search?q=python&type=image

Then, run `docker pull <image>` and `docker run -ti <image>` to access a brand new container, created from this image.

### Exo3: Install new stuff in your container

Now, you have to install everything your job on gitlab will need to run your pipeline. Depending on the image you choose, use the right package container to install all the dependencies. To simplify, the commands you have to run in your container are the same you'd add to the `before_script` section in your `gitlab-ci.yml`. Once you have installed everything you need, just `exit` your container, and run `docker ps -a`. Your container should now appear in the list.

### Exo4: Save the container as an image

The `docker ps` command gives you a name for your container. Given that, run `docker commit <container-name>` to create an image from it.

Run `docker images` to list your local images. You see a new image created few seconds ago...

### Exo5: Push the image on a repository

Now you have created your custom docker image that matches *your* need for the project, you need to publish this image somewhere to make it available. The gitlab runner (that runs your pipelines) will then be able to use it as base image for your jobs.

See [Menu ► Packages & Registries ► Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) for the command to connect/push to your Gitlab docker registry (=docker image repository).

To push your image on the Gitlab docker registry, don't forget to run `docker login <registry_url>` with your gitlab credentials.

Then, push you docker image on your Gitlab docker registry!

For more details on how to push your image, see this [link](https://karlcode.owtelse.com/blog/2017/01/25/push-a-docker-image-to-personal-repository/).

Where to push your image: `index.docker.io/<dockerio-username>/<imagename>`. It must have a specific tag. For more details on how to publish your image in your GitLab project, see in the `Packages & Registries` section of your project, in `Container Registry`.

#### Resources

- If pushing your image results in a "bad password" error message, you can use a [PAT (Personal Access Token)](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html), like explained  [here](https://stackoverflow.com/questions/44149105/docker-login-to-gitlab-registry-with-github-account/44149280#44149280).

### Exo6: Change your pipeline to use your new image

Finally, add the `image: <your-image-id>` to your pipeline configuration (`.gitlab-ci.yaml`) to make gitlab use this image instead of the default one.

### Exo7: Write a DockerFile

There are two ways to create a docker image:

- From an existing container (i.e. creating a snapshot, as you did above)
- From a Dockerfile

A Dockerfile is a configuration file that contains the following information:

- A base docker image (used to create the base container)
- A set of instructions to run inside this base container at startup
- An *entrypoint*, which a command that makes the container running. When this command ends, the container is stopped.

You can for example write a Dockerfile like this:

```yaml
FROM ubuntu
RUN apt-get update
RUN apt-get install -y nginx
ENTRYPOINT ["/usr/sbin/nginx","-g","daemon off;"]
```

From it, docker will pull the `ubuntu` image, then update the container packages, install the `nginx` software and finally run `/usr/bin/nginx -g daemon off;`.

### Resources

- [What is Docker](https://en.wikipedia.org/wiki/Docker_%28software%29)
- [Comprendre Docker](https://docker.avec.une-tasse-de.cafe/)
- [Docker RefCard @DZone](https://dzone.com/refcardz/getting-started-with-docker-1)
- [Docker CheatSheet](https://www.docker.com/sites/default/files/d8/2019-09/docker-cheat-sheet.pdf)
- [Virtualization vs. Emulation vs. Simulation](https://www3.technologyevaluation.com/sd/category/virtualization-virtual-machine/articles/Virtualization-vs-Emulation-vs-Simulation)
