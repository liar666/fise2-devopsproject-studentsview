# Write unit tests for previous code (LOGs)

In the last Lab, you've written new code for extracting informations from the logs of the Apache daemon that runs on the MonitorMe1 machine.

In the Lab before the last Lab, you've selected a framework for Unit Test and written a first suite of tests.

## TODO

- Today, the goal is simple: complete your test suite(s) to include tests for the new code, the one that extracts informations from the logs.
- **At the end of this session, you'll commit your work in your repository and send us a link to the file where you have put your tests. It will be part of the final score of the project.**

## Resources

- Since 11/2020, DockertLab as set up limits on the number of pulls from the Docker Hub (~100 pulls / 6h / IP). One solution is to create an account on the Docker Hub and configure the CI to login on the Docker Hub (limit then raises to ~200 pulls / 6h / account). See: <https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#authenticating-with-registry-in-docker-in-docker> . 
- NOTE: GitLab use the `service` directive with a 'dind' (a.k.a. Docker-in-Docker image). See : <https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service
